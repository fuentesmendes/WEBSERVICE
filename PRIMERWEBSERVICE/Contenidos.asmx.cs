﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using MODELO;

namespace PRIMERWEBSERVICE
{
    /// <summary>
    /// Summary description for Contenidos
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Contenidos : System.Web.Services.WebService
    {
        PruebasEntities pe = new PruebasEntities();

        [WebMethod]
        public string NuevoAlumno(string Codigo, string Nombres, string Apellidos, int Edad, string Email, int IdSexo, int IdProfesion, int IdPais)
        {
            try
            {
                pe.sp_NuevoAlumno(Codigo, Nombres, Apellidos, Edad, Email, IdSexo, IdProfesion, IdPais);
                return "Se agrego el nuevo alumno correctamente";
            }
            catch (Exception)
            {
                return "Error al agregar el nuevo alumno";
            }
        }
    }
}
