﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MODELO
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class PruebasEntities : DbContext
    {
        public PruebasEntities()
            : base("name=PruebasEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<T_Alumno> T_Alumno { get; set; }
        public virtual DbSet<T_Empleado> T_Empleado { get; set; }
        public virtual DbSet<T_Llamadas> T_Llamadas { get; set; }
        public virtual DbSet<T_Pais> T_Pais { get; set; }
        public virtual DbSet<T_Profesion> T_Profesion { get; set; }
        public virtual DbSet<T_Sexo> T_Sexo { get; set; }
    
        public virtual int sp_ActualizarAlumnos(string codigo, string nombres, string apellidos, Nullable<int> edad, string email, Nullable<int> idSexo, Nullable<int> idProfesion, Nullable<int> idPais)
        {
            var codigoParameter = codigo != null ?
                new ObjectParameter("Codigo", codigo) :
                new ObjectParameter("Codigo", typeof(string));
    
            var nombresParameter = nombres != null ?
                new ObjectParameter("Nombres", nombres) :
                new ObjectParameter("Nombres", typeof(string));
    
            var apellidosParameter = apellidos != null ?
                new ObjectParameter("Apellidos", apellidos) :
                new ObjectParameter("Apellidos", typeof(string));
    
            var edadParameter = edad.HasValue ?
                new ObjectParameter("Edad", edad) :
                new ObjectParameter("Edad", typeof(int));
    
            var emailParameter = email != null ?
                new ObjectParameter("Email", email) :
                new ObjectParameter("Email", typeof(string));
    
            var idSexoParameter = idSexo.HasValue ?
                new ObjectParameter("IdSexo", idSexo) :
                new ObjectParameter("IdSexo", typeof(int));
    
            var idProfesionParameter = idProfesion.HasValue ?
                new ObjectParameter("IdProfesion", idProfesion) :
                new ObjectParameter("IdProfesion", typeof(int));
    
            var idPaisParameter = idPais.HasValue ?
                new ObjectParameter("IdPais", idPais) :
                new ObjectParameter("IdPais", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_ActualizarAlumnos", codigoParameter, nombresParameter, apellidosParameter, edadParameter, emailParameter, idSexoParameter, idProfesionParameter, idPaisParameter);
        }
    
        public virtual int sp_EliminarAlumnos(string codigo)
        {
            var codigoParameter = codigo != null ?
                new ObjectParameter("Codigo", codigo) :
                new ObjectParameter("Codigo", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_EliminarAlumnos", codigoParameter);
        }
    
        public virtual int sp_InsertaAlumnos(string codigo, string nombres, string apellidos, Nullable<int> edad, string email, Nullable<int> idSexo, Nullable<int> idProfesion, Nullable<int> idPais)
        {
            var codigoParameter = codigo != null ?
                new ObjectParameter("Codigo", codigo) :
                new ObjectParameter("Codigo", typeof(string));
    
            var nombresParameter = nombres != null ?
                new ObjectParameter("Nombres", nombres) :
                new ObjectParameter("Nombres", typeof(string));
    
            var apellidosParameter = apellidos != null ?
                new ObjectParameter("Apellidos", apellidos) :
                new ObjectParameter("Apellidos", typeof(string));
    
            var edadParameter = edad.HasValue ?
                new ObjectParameter("Edad", edad) :
                new ObjectParameter("Edad", typeof(int));
    
            var emailParameter = email != null ?
                new ObjectParameter("Email", email) :
                new ObjectParameter("Email", typeof(string));
    
            var idSexoParameter = idSexo.HasValue ?
                new ObjectParameter("IdSexo", idSexo) :
                new ObjectParameter("IdSexo", typeof(int));
    
            var idProfesionParameter = idProfesion.HasValue ?
                new ObjectParameter("IdProfesion", idProfesion) :
                new ObjectParameter("IdProfesion", typeof(int));
    
            var idPaisParameter = idPais.HasValue ?
                new ObjectParameter("IdPais", idPais) :
                new ObjectParameter("IdPais", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_InsertaAlumnos", codigoParameter, nombresParameter, apellidosParameter, edadParameter, emailParameter, idSexoParameter, idProfesionParameter, idPaisParameter);
        }
    
        public virtual int sp_NuevoAlumno(string cod, string nom, string apell, Nullable<int> eda, string correo, Nullable<int> sexo, Nullable<int> profesion, Nullable<int> pais)
        {
            var codParameter = cod != null ?
                new ObjectParameter("Cod", cod) :
                new ObjectParameter("Cod", typeof(string));
    
            var nomParameter = nom != null ?
                new ObjectParameter("nom", nom) :
                new ObjectParameter("nom", typeof(string));
    
            var apellParameter = apell != null ?
                new ObjectParameter("apell", apell) :
                new ObjectParameter("apell", typeof(string));
    
            var edaParameter = eda.HasValue ?
                new ObjectParameter("eda", eda) :
                new ObjectParameter("eda", typeof(int));
    
            var correoParameter = correo != null ?
                new ObjectParameter("correo", correo) :
                new ObjectParameter("correo", typeof(string));
    
            var sexoParameter = sexo.HasValue ?
                new ObjectParameter("sexo", sexo) :
                new ObjectParameter("sexo", typeof(int));
    
            var profesionParameter = profesion.HasValue ?
                new ObjectParameter("profesion", profesion) :
                new ObjectParameter("profesion", typeof(int));
    
            var paisParameter = pais.HasValue ?
                new ObjectParameter("pais", pais) :
                new ObjectParameter("pais", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_NuevoAlumno", codParameter, nomParameter, apellParameter, edaParameter, correoParameter, sexoParameter, profesionParameter, paisParameter);
        }
    }
}
